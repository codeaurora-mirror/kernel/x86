/*
 * platform_lp5562.h:  lp5562 platform data header file
 *
 * (C) Copyright 2014 Intel Corporation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; version 2
 * of the License.
 */

#ifndef _PLATFORM_LP5562_
#define _PLATFORM_LP5562_

extern void *lp55xx_platform_data(void *info)__attribute__((weak));

#endif
